/* -------------------
 * Author: Guy Upton
 * Last edit (DD/MM/YYYY): 29/01/2020 
 */

package testing;

import static org.junit.Assert.*;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import production.PostcodeInfoFinder;

public class PostCodeInfoFinderTester {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testValidatePos() {
		
		assertTrue(PostcodeInfoFinder.validate("CB30FA"));
		
	}
	
	@Test
	public void testValidateNeg() {
		
		assertFalse(PostcodeInfoFinder.validate("FAIL"));
		
	}
	
	@Test
	public void testGetDataPos() throws ParseException {
		
		JSONParser parser = new JSONParser(); 
		JSONObject json = (JSONObject) parser.parse("{\"status\":200,\"result\":{\"postcode\":\"CB3 0FA\",\"quality\":1,\"eastings\":542975,\"northings\":258918,\"country\":\"England\",\"nhs_ha\":\"East of England\",\"longitude\":0.091115,\"latitude\":52.209953,\"european_electoral_region\":\"Eastern\",\"primary_care_trust\":\"Cambridgeshire\",\"region\":\"East of England\",\"lsoa\":\"Cambridge 007D\",\"msoa\":\"Cambridge 007\",\"incode\":\"0FA\",\"outcode\":\"CB3\",\"parliamentary_constituency\":\"Cambridge\",\"admin_district\":\"Cambridge\",\"parish\":\"Cambridge, unparished area\",\"admin_county\":\"Cambridgeshire\",\"admin_ward\":\"Newnham\",\"ced\":\"Newnham\",\"ccg\":\"NHS Cambridgeshire and Peterborough\",\"nuts\":\"Cambridgeshire CC\",\"codes\":{\"admin_district\":\"E07000008\",\"admin_county\":\"E10000003\",\"admin_ward\":\"E05002710\",\"parish\":\"E43000042\",\"parliamentary_constituency\":\"E14000617\",\"ccg\":\"E38000026\",\"ccg_id\":\"06H\",\"ced\":\"E58000080\",\"nuts\":\"UKH12\"}}}");
		
		assertEquals(json, PostcodeInfoFinder.getData("CB30FA"));
		
	}

	@Test
	public void testGetDataNeg() {
		
		assertNull(PostcodeInfoFinder.getData(null));
		
		
	}
	
	@Test
	public void testGetDataAdditionPos() throws ParseException {
		
		JSONParser parser = new JSONParser(); 
		JSONObject json = (JSONObject) parser.parse("{\"status\":200,\"result\":true}");
		
		assertEquals(json, PostcodeInfoFinder.getData("CB30FA", "/validate"));
		
	}
	
	@Test 
	public void testGetDataAdditionNeg() {
		
		
		assertNull(PostcodeInfoFinder.getData("CB30FA", "hdsfkjds"));
		
		
	}

}
