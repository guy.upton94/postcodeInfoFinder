/* -------------------
 * Author: Guy Upton
 * Last edit (DD/MM/YYYY): 29/01/2020 
 */

package production;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class PostcodeInfoFinder {
	
	//private static String thePostcode = "";
	
	public static void main(String [] args){
		String thePostcode;
		String incorrectUse = "Incorrect use of program. Pass at least one postcode. All postcodes should be valid UK codes.";
		String validationFail = "Failed to validate the postcode";
		
		
		
		if(args.length != 0) {
			
			//Get the postcode from the passed parameter and remove all whitespace
			thePostcode = args[0].replaceAll("\\s+","");;
			
			
			//If a valid postcode then perform the data retrieval, otherwise display error
			if(validate(thePostcode)) {
				System.out.println("given postcode: " + thePostcode);
				displayPcData(thePostcode);
				displayNearPcData(thePostcode);
			}else {
				
				System.out.println(validationFail);
			}
		}else {
			System.out.println(incorrectUse);
		}
	}
	

	
	/**
	 * @return true if the postcode is valid, if not false
	 */
	public static boolean validate(String thePostcode) {
		
		//Get the JSONObj
		JSONObject returnedData = getData(thePostcode, "/validate");
 
		//If a obj has been returned and its of the correct status, and the result is true then return true
		if(returnedData != null) {
			if(returnedData.get("status").toString().equals("200") && 
					returnedData.get("result").toString().equals("true")) {
				return true;			
			}
		}
		
		return false;
		
	}
	
	/**
	 * Displays the data of the postcodes near the given postcode
	 */
	public static void displayNearPcData(String thePostcode) {
		
		System.out.println("Close postcodes:");
		
		//Get obj with a radius from constants
		JSONObject returnedData = getData(thePostcode, "/nearest?radius=" + Constants.RADIUS);
		
		if(returnedData != null) {
			JSONArray jsonArray = (JSONArray) (returnedData.get("result"));
			jsonArray.remove(0);
			
			for(Object ca : jsonArray) {
				JSONObject obj = (JSONObject) ca;
				displayData(obj, new String[] {"postcode"});
				
			}
		}
	
	}
	
	
	/**
	 * Displays the Postcode Data
	 */
	public static void displayPcData(String thePostcode) {
		
		JSONObject returnedData = getData(thePostcode);
		if(returnedData != null) {
			displayData((JSONObject) returnedData.get("result"), new String[] {"country", "region"});
		}
	}
	
	
	/**
	 * Displays the wanted data from a given JSON obj
	 */
	private static void displayData(JSONObject theJSONObj, String[] keys) {
		
		/*
		 * for(String i : keys) { System.out.format("%32s,%32s", i, theJSONObj.get); }
		 */
		
		
		String out = "";
		
		for(String i : keys) {
			out += i + ": " + theJSONObj.get(i) + "\n";
		}
		
		System.out.println(out);
	}
	
	/**
	 * @Param Assumes a valid postcode
	 * @return a JSON obj or null
	 */
	public static JSONObject getData(String thePostcode) {
		return getData(thePostcode, "");
	}
	
	/**
	 * @Param Assumes a valid postcode
	 * @return a JSON obj or null
	 */
	public static JSONObject getData(String thePostcode, String additions) {
		

		URL postcodeURL;
		
        BufferedReader in;
		try {
			
			postcodeURL = new URL(Constants.MAIN_URL_STRING + thePostcode + additions);
			
			HttpURLConnection conn = (HttpURLConnection) postcodeURL.openConnection();
			conn.setConnectTimeout(Constants.TIMEOUT_TIME);
		    conn.setRequestMethod("GET");
		      
			in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			
			String webData = in.readLine();

	        in.close();
	        JSONParser parser = new JSONParser();
	        return (JSONObject) parser.parse(webData);
		        
		} catch(SocketTimeoutException e) {
			System.err.println("Page timed out");
			
		} catch (IOException e) {
			System.err.println("Failed to get data from webpage");
			
		} catch (ParseException e) {
			System.err.println("Failed to parse JSON obj");
			
		} 
		return null;
	}

}
