/* -------------------
 * Author: Guy Upton
 * Last edit (DD/MM/YYYY): 28/01/2020 
 */


package production;

public class Constants {
	
	public final static String MAIN_URL_STRING = "https://api.postcodes.io/postcodes/";
	
	
	//Radius to search for close postcodes from postcode specified. In meters.
	public final static int RADIUS = 500;
	
	public final static int TIMEOUT_TIME = 1000;

}
